/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.HelloSpring.resource;

import com.andersoney.HelloSpring.Model.HelloWord;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.andersoney.HelloSpring.repository.HelloWords;

/**
 *
 * @author ander
 */
@RestController
public class HelloWordsResource {

    @Autowired
    private HelloWords hellowords;// <<< Repositório de produtos.

    @GetMapping("/cursos/{id}")
    public HelloWord buscar(@PathVariable Long id) {
        return hellowords.findOne(id);
    }

    @GetMapping("/cursos")
    public List<HelloWord> pesquisar() {
        return hellowords.findAll();
    }

    @PostMapping("/cursos")
    public HelloWord salvar(@RequestBody HelloWord curso) {
        return hellowords.save(curso);
    }

    @DeleteMapping("/cursos/{id}")
    public void deletar(@PathVariable Long id) {
        hellowords.delete(id);
    }

}
